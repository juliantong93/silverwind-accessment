import axios from 'axios'

let http = {}
http.get = function(url) {
  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(res => {
        resolve(res.data)
      })
      .catch(e => {
        reject()
        console.warn('Axios error: ' + e)
      })
  })
}

export default http
