import { createApp } from 'vue'
import Router from './router'
import Directives from '@directives'
import App from './App.vue'

createApp(App)
  .use(Router)
  .use(Directives)
  .mount('#app')

