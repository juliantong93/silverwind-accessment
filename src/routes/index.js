import HomePage from '@pages'

const routes = [
  {
    path: "/",
    name: "HomePage",
    component: HomePage,
    meta: {
      layout: 1,
    },
  },
  {
    path: "/diamond",
    name: "Diamond",
    component: () => import('@pages/diamond'),
    meta: {
      layout: 2,
    },
  },
]

export default routes
