const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  runtimeCompiler: true,
  productionSourceMap: false,
  chainWebpack: config => {
    config.resolve.alias
      .set('@routes', resolve('src/routes'))
      .set('@utils', resolve('src/utils'))
      .set('@api', resolve('src/api'))
      .set('@directives', resolve('src/directives'))
      .set('@layout', resolve('src/layout'))
      .set('@pages', resolve('src/pages'))
      .set('@components', resolve('src/components'))
  }
}
